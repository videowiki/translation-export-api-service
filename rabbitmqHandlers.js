const {
  websocketsService,
  websocketsEvents,
  articleService,
  subtitlesService,
  userService,
  storageService,
  organizationService,
  emailService,
} = require("./services");

const TranslationExport = require("./models").TranslationExport;
const fs = require("fs");

const BULK_EXPORT_DIRECTORY = "bulkExports";

const queues = require("@videowiki/workers/vendors/rabbitmq/queues");
const { BulkTranslationExport } = require("./models");

let rabbitmqChannel;
const archiver = require('archiver');
const request = require('request');

function generateZipFile(zipFileName, data) {
  return new Promise((resolve, reject) => {
    const path = `${__dirname}/${zipFileName}_${Date.now()}.zip`;
    const output = fs.createWriteStream(path);
    const archive = archiver("zip", {
      zlib: { level: 9 },
    });
    archive.pipe(output);
    data.forEach((d) => {
      archive.append(request(d.url), { name: d.fileName });
    });
    archive.finalize();
    output.on("close", () => {
      resolve(path);
    });
    archive.on("error", (err) => {
      reject(err);
    });
  });
}
function init(channel) {
  rabbitmqChannel = channel;
  rabbitmqChannel.prefetch(1);
  rabbitmqChannel.consume(
    queues.BURN_ARTICLE_TRANSLATION_VIDEO_SUBTITLE_AND_SIGNLANGUAGE_FINISH,
    onBurnSignlanguageFinish,
    { noAck: false }
  );
  rabbitmqChannel.consume(
    queues.ARCHIVE_ARTICLE_TRANSLATION_AUDIOS_FINISH,
    onArchiveAudiosFinish,
    { noAck: false }
  );
  rabbitmqChannel.consume(
    queues.GENERATE_ARTICLE_TRANSLATION_VIDEO_SUBTITLE_FINISH,
    onGenerateSubtitleFinish,
    { noAck: false }
  );
  rabbitmqChannel.consume(
    queues.BURN_ARTICLE_TRANSLATION_VIDEO_SUBTITLE_FINISH,
    onBurnSubtitlesFinish,
    { noAck: false }
  );
  rabbitmqChannel.consume(
    queues.EXPORT_ARTICLE_TRANSLATION_FINISH,
    onExportTranslationFinish,
    { noAck: false }
  );
}

function onExportTranslationFinish(msg) {
  const { translationExportId } = JSON.parse(msg.content.toString());
  console.log("onExportTranslationFinish", translationExportId);
  rabbitmqChannel.ack(msg);

  addTranslationExportToBulkExport(translationExportId);
  createSubtitleForTranslationExport(translationExportId);
}

function addTranslationExportToBulkExport(translationExportId) {
  BulkTranslationExport.findOne({ translationExportIds: translationExportId })
    .then((bulkTranslationExport) => {
      if (bulkTranslationExport) {
        return BulkTranslationExport.findOneAndUpdate(
          { _id: bulkTranslationExport._id },
          { $addToSet: { finishedTranslationExportIds: translationExportId } },
          { new: true }
        )
          .then((updatedBulkTranslationExport) => {
            if (
              updatedBulkTranslationExport.translationExportIds.length ===
              updatedBulkTranslationExport.finishedTranslationExportIds.length
            ) {
              let organization;
              let zippedFilePath;
              let zipUrl;
              organizationService
                .findOne({ _id: updatedBulkTranslationExport.organization })
                .then((org) => {
                  organization = org;
                  return TranslationExport.find({
                    _id: {
                      $in:
                        updatedBulkTranslationExport.finishedTranslationExportIds,
                    },
                  });
                })
                .then((translationExports) => {
                  const zipData = translationExports.map((te) => {
                    return {
                      url: te.videoUrl,
                      fileName: te.videoUrl.split("/").pop(),
                    };
                  });
                  return generateZipFile(organization.name, zipData);
                })
                .then((zfp) => {
                  zippedFilePath = zfp;
                  return storageService.saveFile(
                    BULK_EXPORT_DIRECTORY,
                    zippedFilePath.split("/").pop(),
                    fs.createReadStream(zippedFilePath)
                  );
                })
                .then(({ url }) => {
                  zipUrl = url;
                  console.log("the uploaded zip file url ", url);
                  fs.unlink(zippedFilePath, (err) => {
                    if (err) console.log("removed zip file", err);
                  });
                  return userService.findOne({
                    _id: updatedBulkTranslationExport.exportBy,
                  });
                })
                .then((user) => {
                  return emailService.sendBulkExportTranslationsZipFile({
                    to: user,
                    organizationName: organization.name,
                    zipUrl,
                  });
                })
                .then(() => {
                  console.log("bulk translation export and download finished");
                })
                .catch((err) => {
                  console.log(err);
                });
            }
          })
          .catch((err) => {
            console.log(err);
          });
      } else {
        console.log("no bulk translation export associated");
      }
    })
    .catch((err) => {
      console.log(err);
    });
}

function createSubtitleForTranslationExport(translationExportId) {
  let translationExport;
  let article;
  TranslationExport.findById(translationExportId)
    .then((translationExportDoc) => {
      if (!translationExportDoc)
        throw new Error("Invalid translation export id" + translationExportId);
      translationExport = translationExportDoc.toObject();
      if (translationExport.status === "failed")
        throw new Error("Failed translation export");
      return articleService.findById(translationExport.article);
    })
    .then((articleDoc) => {
      article = articleDoc.toObject();
      translationExport.article = article;
      return subtitlesService.find({ article: translationExport.article._id });
    })
    .then((subtitles) => {
      // if there's no subtitles generated after the first export, generate one
      if (!subtitles || subtitles.length === 0) {
        const subtitles = subtitlesService.generateSubtitlesFromSlides(
          article.slides
        );
        const newSubtitles = {
          article: article._id,
          organization: article.organization,
          video: article.video,
          subtitles,
        };
        return subtitlesService.create(newSubtitles);
      }
    })
    .then(() => {
      console.log("onExportTranslationFinish done");
    })
    .catch((err) => {
      console.log("error onExportTranlationFinish", err);
    });
}

function onArchiveAudiosFinish(msg) {
  const { translationExportId } = JSON.parse(msg.content.toString());
  let translationExport;
  rabbitmqChannel.ack(msg);

  TranslationExport.findById(translationExportId)
    .then((translationExportDoc) => {
      if (!translationExportDoc)
        throw new Error("Invalid translation export id");
      translationExport = translationExportDoc.toObject();
      if (!translationExport.audioArchiveBy)
        throw new Error("No one requested to download that");
      return userService.findById(translationExport.audioArchiveBy);
    })
    .then((user) => {
      websocketsService.emitEvent({
        _id: user._id,
        event: websocketsEvents.DOWNLOAD_FILE,
        data: { url: translationExport.audiosArchiveUrl },
      });
    })
    .catch((err) => {
      console.log(err);
    });
}

function onGenerateSubtitleFinish(msg) {
  const { translationExportId } = JSON.parse(msg.content.toString());
  let translationExport;
  rabbitmqChannel.ack(msg);
  TranslationExport.findById(translationExportId)
    .then((translationExportDoc) => {
      if (!translationExportDoc)
        throw new Error("Invalid translation export id");
      translationExport = translationExportDoc.toObject();
      if (!translationExport.subtitleBy)
        throw new Error("No one requested to download that");
      return userService.findById(translationExport.subtitleBy);
    })
    .then((user) => {
      websocketsService.emitEvent({
        _id: user._id,
        event: websocketsEvents.DOWNLOAD_FILE,
        data: { url: translationExport.subtitleUrl },
      });
    })
    .catch((err) => {
      console.log(err);
    });
}

function onBurnSubtitlesFinish(msg) {
  const { translationExportId } = JSON.parse(msg.content.toString());
  let translationExport;
  rabbitmqChannel.ack(msg);
  console.log("downloading burn video");
  TranslationExport.findById(translationExportId)
    .then((translationExportDoc) => {
      if (!translationExportDoc)
        throw new Error("Invalid translation export id");
      translationExport = translationExportDoc.toObject();
      if (!translationExport.subtitledVideoBy)
        throw new Error("No one requested to download that");
      return userService.findById(translationExport.subtitledVideoBy);
    })
    .then((user) => {
      websocketsService.emitEvent({
        _id: user._id,
        event: websocketsEvents.DOWNLOAD_FILE,
        data: { url: translationExport.subtitledVideoUrl },
      });
    })
    .catch((err) => {
      console.log(err);
    });
}

function onBurnSignlanguageFinish(msg) {
  const { translationExportId } = JSON.parse(msg.content.toString());
  let translationExport;
  rabbitmqChannel.ack(msg);
  console.log("on burn signlanguage finish ");
  TranslationExport.findById(translationExportId)
    .then((translationExportDoc) => {
      if (!translationExportDoc)
        throw new Error("Invalid translation export id");
      translationExport = translationExportDoc.toObject();
      if (!translationExport.subtitledSignlanguageVideoBy)
        throw new Error("No one requested to download that");
      return userService.findById(
        translationExport.subtitledSignlanguageVideoBy
      );
    })
    .then((user) => {
      websocketsService.emitEvent({
        _id: user._id,
        event: websocketsEvents.DOWNLOAD_FILE,
        data: { url: translationExport.subtitledSignlanguageVideoUrl },
      });
    })
    .catch((err) => {
      console.log(err);
    });
}

module.exports = {
  init,
};
