const { RABBITMQ_SERVER, AUDIO_PROCESSOR_API_ROOT } = process.env;

module.exports = ({ rabbitmqChannel }) => {
  const exporterWorker = require("@videowiki/workers/exporter")({
    rabbitmqChannel,
  });
  const audioProcessorWorker = require("@videowiki/workers/audio_processor")({
    rabbitmqChannel,
    AUDIO_PROCESSOR_API_ROOT,
  });
  return {
    exporterWorker,
    audioProcessorWorker,
  };
};
