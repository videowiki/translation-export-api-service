module.exports = {
    create_video: function(req, res, next) {
        const { title, langCode, numberOfSpeakers } = req.body;
        if (!title) {
            return res.status(400).send('title field is required');
        }
        if (!langCode) {
            return res.status(400).send('langCode field is required');
        }
        if (!numberOfSpeakers) {
            return res.status(400).send('speakers field is required');
        }
        // TODO Validate video title
        return next();
    }
}