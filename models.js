const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EXPORT_REQUEST_STATUS = ['pending', 'approved', 'declined'];
const STATUS_ENUM = ['queued', 'processing', 'done', 'failed'];

const TranslationExportSchema = new Schema({
    organization: { type: Schema.Types.ObjectId },
    article: { type: Schema.Types.ObjectId },
    video: { type: Schema.Types.ObjectId },
    signLanguageArticle: { type: Schema.Types.ObjectId },

    exportRequestStatus: { type: String, enum: EXPORT_REQUEST_STATUS, default: 'pending' },

    exportRequestBy: { type: Schema.Types.ObjectId },
    translationBy: [{ type: Schema.Types.ObjectId }],
    approvedBy: { type: Schema.Types.ObjectId },
    declinedBy: { type: Schema.Types.ObjectId },

    progress: { type: Number, default: 0 },
    status: { type: String, enum: STATUS_ENUM },

    videoUrl: { type: String },
    compressedVideoUrl: { type: String },
    slidesArchiveUrl: { type: String },
    audiosArchiveUrl: { type: String },
    subtitledVideoUrl: { type: String },
    subtitledSignlanguageVideoUrl: { type: String },

    audioArchiveBy: { type: Schema.Types.ObjectId },
    subtitledVideoBy: { type: Schema.Types.ObjectId },
    subtitleBy: { type: Schema.Types.ObjectId },
    subtitledSignlanguageVideoBy: { type: Schema.Types.ObjectId },
    
    audiosArchiveProgress: { type: Number, default: 0 },
    subtitledVideoProgress: { type: Number, default: 0 },
    subtitleUrl: { type: String },
    subtitleProgress: { type: Number },
    subtitledSignlanguageVideoProgress: { type: Number, default: 0 },

    voiceVolume: { type: Number, default: 1 },
    backgroundMusicVolume: { type: Number, default: 1 },
    normalizeAudio: { type: Boolean, default: true },
    cancelNoise: { type: Boolean, default: true },

    backgroundMusicTransposed: { type: Boolean, default: false },
    hasBackgroundMusic: { type: Boolean, default: false },

    version: { type: Number, default: 1 },
    subVersion: { type: Number, default: 0 },

    created_at: { type: Date, default: Date.now, index: true },
    // Directory name for generated media to be saved in
    dir: { type: String },
})

const BulkTranslationExportSchema = new Schema({
    organization: { type: Schema.Types.ObjectId },
    translationExportIds: [{ type: Schema.Types.ObjectId }],
    finishedTranslationExportIds: [{ type: Schema.Types.ObjectId }],
    exportBy: { type: Schema.Types.ObjectId },
    zipUrl: { type: String }
})

const TranslationExport = mongoose.model('translationExport', TranslationExportSchema);
const BulkTranslationExport = mongoose.model('bulkTranslationExport', BulkTranslationExportSchema);

module.exports = { TranslationExport, BulkTranslationExport };