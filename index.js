const { server, app, createRouter } = require('./generateServer')();
const middlewares = require('./middlewares');
const videowikiGenerators = require('@videowiki/generators');

const rabbitmqService = require('@videowiki/workers/vendors/rabbitmq')
const RABBITMQ_SERVER = process.env.RABBITMQ_SERVER;
const mongoose = require('mongoose');
const DB_CONNECTION = process.env.TRANSLATION_EXPORT_SERVICE_DATABASE_URL;
let mongoConnection;

mongoose.connect(DB_CONNECTION)
.then(con => {
    mongoConnection = con.connection;
    con.connection.on('disconnected', () => {
        console.log('Database disconnected! shutting down service')
        process.exit(1);
    })
    rabbitmqService.createChannel(RABBITMQ_SERVER, (err, channel) => {
        if (err) {
            throw err;
        }

        channel.on('error', (err) => {
            console.log('RABBITMQ ERROR', err)
            process.exit(1);
        })
        channel.on('close', () => {
            console.log('RABBITMQ CLOSE')
            process.exit(1);
        })

        videowikiGenerators.healthcheckRouteGenerator({ router: app, mongoConnection, rabbitmqConnection: channel.connection })
        
        const workers = require('./workers')({ rabbitmqChannel: channel });
        const controller = require('./controller')({ workers });
        
        require('./rabbitmqHandlers').init(channel)

        app.use('/db', require('./dbRoutes')(createRouter()))

        app.all('*', (req, res, next) => {
            if (req.headers['vw-user-data']) {
                try {
                    const user = JSON.parse(req.headers['vw-user-data']);
                    req.user = user;
                } catch (e) {
                    console.log(e);
                }
            }
            next();
        })
        
        app.get('/by_article_id/:articleId', controller.getByArticleId);
        
        app.post('/requestExport', middlewares.authorizeRequestExport, controller.exportTranslationRequest);
        app.post('/requestExportMultiple', middlewares.authorizeRequestExportMultiple, controller.exportMultipleTranslationRequest);
        app.post('/:translationExportId/approve', middlewares.authorizeApproveAndDecline, controller.approveTranslationExport);
        app.post('/:translationExportId/decline', middlewares.authorizeApproveAndDecline, controller.declineTranslationExport);
        app.post('/:translationExportId/audios/generateArchive', middlewares.validateArchiveAudios, controller.archiveAudios);
        app.post('/:translationExportId/video/subtitles', middlewares.validateGenerateSubtitles, controller.generateVideoSubtitle);
        app.post('/:translationExportId/video/burnSubtitles', middlewares.validateBurnSubtitles, controller.burnVideoSubtitle);
        app.post('/:translationExportId/video/burnSubtitlesSignlanguage', controller.burnVideoSubtitleAndSignlanguage);
        app.put('/:translationExportId/audioSettings', middlewares.authorizeApproveAndDecline, controller.updateAudioSettings);
        
    })
})
.catch(err => {
    console.log('mongo connection error', err);
    process.exit(1);
})


const PORT = process.env.PORT || 4000;
server.listen(PORT)
console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
exports = module.exports = app             // expose app
