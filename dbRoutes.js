
const TranslationExport = require('./models').TranslationExport;

module.exports = router => {

    router.get('/count', (req, res) => {
        console.log('hello count')
        TranslationExport.count(req.query)
            .then(count => res.json(count))
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message)
            })

    })



    // CRUD routes
    router.get('/', (req, res) => {
        const { sort, skip, limit, one, ...rest } = req.query;
        let q;
        if (!rest || Object.keys(rest || {}).length === 0) {
            return res.json([]);
        }

        Object.keys(rest).forEach((key) => {
            if (key.indexOf('$') === 0) {
                const val = rest[key];
                rest[key] = [];
                Object.keys(val).forEach((subKey) => {
                    val[subKey].forEach(subVal => {
                        rest[key].push({ [subKey]: subVal })
                    })
                })
            }
        })
        if (one) {
            q = TranslationExport.findOne(rest);
        } else {
            q = TranslationExport.find(rest);
        }
        if (sort) {
            Object.keys(sort).forEach(key => {
                q.sort({ [key]: parseInt(sort[key]) })
            })
        }
        if (skip) {
            q.skip(parseInt(skip))
        }
        if (limit) {
            q.limit(parseInt(limit))
        }
        q.then((translationExports) => {
            return res.json(translationExports);
        })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.post('/', (req, res) => {
        const data = req.body;
        TranslationExport.create(data)
            .then((translationExport) => {
                return res.json(translationExport);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/', (req, res) => {
        let { conditions, values, options } = req.body;
        if (!options) {
            options = {};
        }
        TranslationExport.update(conditions, { $set: values }, { ...options, multi: true })
            .then(() => TranslationExport.find(conditions))
            .then(translationExports => {
                return res.json(translationExports);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/', (req, res) => {
        let conditions = req.body;
        let translationExports;
        TranslationExport.find(conditions)
            .then((a) => {
                translationExports = a;
                return TranslationExport.remove(conditions)
            })
            .then(() => {
                return res.json(translationExports);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.get('/:id', (req, res) => {
        TranslationExport.findById(req.params.id)
            .then((translationExport) => {
                return res.json(translationExport);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/:id', (req, res) => {
        const { id } = req.params;
        const changes = req.body;
        TranslationExport.findByIdAndUpdate(id, { $set: changes })
            .then(() => TranslationExport.findById(id))
            .then(translationExport => {
                return res.json(translationExport);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/:id', (req, res) => {
        const { id } = req.params;
        let deletedTranslationExport;
        TranslationExport.findById(id)
            .then(translationExport => {
                deletedTranslationExport = translationExport;
                return TranslationExport.findByIdAndRemove(id)
            })
            .then(() => {
                return res.json(deletedTranslationExport);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })


    return router;
}